/**
 * Created by zlintz on 1/14/16.
 */

(function (dbu) {
  /**
   * Private vars
   */

  var _EMPTY_OPTION_TEXT = 'Select' // use dbu.setEmptyOptionText to change this value
  var _excludeTagList = ['script', 'style'] // use dbu.addElementsToExcludeList to add additional elements to the exclude list
  /*
      The _excludeTagList include 'script' and 'style' by default because XSS is a high risk with 'script' and website defamation is a
      high risk with 'style'. Additionally these should be residing in there appropriate files anyways and not inline.
  */

  /******************************
   * Public utility functions
   *******************************/

  dbu.addElementsToExcludeList = function (elements) {
    if (Array.isArray(elements) && elements.length) {
      // I am expecting only strings to be in the _excludeTagList array, however the toString().split("'") will enforce this
      Array.prototype.push.apply(_excludeTagList, elements.toString().split(','))
    } else if (typeof elements === 'string') {
      _excludeTagList[_excludeTagList.length] = elements
    } else {
      _logIfAble('Could not determine how to handle [' + elements + ']. Make sure it is a string or an array of strings.')
    }
  }

  /**
   *
   * Builds a element node with no attributes and some display text.  This method will not allow script tags
   * to be created or anything in the _excludeTagList and will return null in this scenario.
   *
   * @param {String} elementType
   * @param {String} displayValue
   * @returns {Element}
   */
  dbu.elementWithText = function (elementType, displayValue) {
    var element = null
    if (!_excludeTagList.includes(elementType)) {
      element = document.createElement(elementType)
      if (typeof displayValue !== 'undefined') {
        element.appendChild(document.createTextNode(displayValue.toString()))
      }
    } else {
      _logIfAble('The following elementType is not allowed [' + elementType + ']. Elements that are not allowed include [' + _excludeTagList.toString() + ']')
    }
    return element
  }

  /**
   *
   * Helper function to build an element with optional displayText
   * and add attributes to the element based on the map.key
   * as the attribute name and the map.value as the attribute value.
   *
   * This function additionally check if the attribute that is being
   * added is an inline event and will skip it if so.
   *
   * @param {String} elementToMake
   * @param {map} attributeMap
   * @returns {Element}
   */
  dbu.elementWithAttributes = function (elementToMake, attributeMap, displayText) {
    var element = dbu.elementWithText(elementToMake, displayText)

    if (element !== null) {
      var callback = function (key, value) {
        if (!_isInlineEventAttribute(key)) {
          // We only want to add attributes that are not inline events
          element.setAttribute(key, value)
        }
      }

      _simpleKeyValueLooper(attributeMap, callback)

      if (elementToMake === 'a') {
        _makeAnchorSafe(element)
      }
    }

    return element
  }

  /**
   * Public: 'option' element specific helper functions
   */

  /**
   *
   * @param parentNode node to append the options to
   * @param startNum starting value, defaults to 0
   * @param optionCount number of option elements to build and add to the parent node, defaults to 1
   * @param step the number to increment the, defaults to 1
   */
  dbu.simpleOptionBuilder = function (parentNode, startNum, optionCount, step) {
    startNum = _defaultValue(startNum, 0)
    optionCount = _defaultValue(optionCount, 1)
    step = _defaultValue(step, 1)

    for (var i = 0; i <= optionCount; i++) {
      parentNode.appendChild(dbu.elementWithAttributes('option', {value: startNum}))
      startNum += step
    }
  }

  /**
   *
   * Builds a select option nodes from a map and then append to
   * the provided parentNone using the map.key as value and the
   *    map.value and the display text.
   *
   * @param {node} parentNode
   * @param {map} dataMap
   * @returns {undefined}
   */
  dbu.optionBuildAndAppend = function (parentNode, dataMap) {
    if (parentNode && dataMap) {
      var callback = function (key, val) {
        var optionNode = document.createElement('option')
        optionNode.value = key
        optionNode.appendChild(document.createTextNode(val))
        parentNode.appendChild(optionNode)
      }

      _simpleKeyValueLooper(dataMap, callback)
    }
  }

  dbu.setEmptyOptionText = function (emptyOptionText) {
    _EMPTY_OPTION_TEXT = _defaultValue(emptyOptionText, '')
  }

  dbu.EMPTY_OPTION = function () {
    var defaultOptionNode = document.createElement('option')
    defaultOptionNode.appendChild(document.createTextNode(_EMPTY_OPTION_TEXT))
    return defaultOptionNode
  }

  /**
   * Anchor
   */

  /**
   *
   * Helps build a basic anchor tag and will prevent the text javascript from being in the
   * url, as extra gaurd against XSS.
   *
   * @param {String} link
   * @param {String} displayText Optional text to use for display. If not provided the link will be used as the display text value.
   * @returns {Element}]
   */
  dbu.anchor = function (link, displayText) {
    var _notEmpty = function (value) {
      return value !== undefined &&
        value !== 'undefined' &&
        value !== null &&
        value !== 'null' &&
        value.toString().trim().length > 0
    }

    if (!displayText || _notEmpty(displayText)) {
      displayText = link
    }
    return dbu.elementWithAttributes('a', {href: link})
  }

  /******************************
   * Private utility functions
   *******************************/

  /**
   * Single depth key/value pair looping against a callback
   * @param object
   * @param callback - assumes that the callback will take the key as the first param and the value as the second param
   * @private
   */
  var _simpleKeyValueLooper = function (obj, callback) {
    /**
     * This allows us to not be dependent on jquery for something like the following
     *
     * $.each(attributeMap, function (key, value) {
         element.setAttribute(key, value);
       });
     */
    if (typeof obj === 'object' && callback) {
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
          callback(key, obj[key])
        }
      }
    }
  }

  /**
   * We will not allow the text javascript to be at the start of an href. This is an eager replace from the start of the href
   * @param anchorElement
   * @private
   */
  var _makeAnchorSafe = function (anchorElement) {
    if (anchorElement.getAttribute('href').slice(0, 'javascript'.length) === 'javascript') {
      var href = anchorElement.getAttribute('href').replace(new RegExp('(javascript)+(.*)', 'g'), '')
      anchorElement.setAttribute('href', href)
    }
  }

  /**
   * Returns a default value if the original is undefined
   * @param original
   * @param defaultValue
   * @returns {*}
   * @private
   */
  var _defaultValue = function (original, defaultValue) {
    return typeof original !== 'undefined' ? original : defaultValue
  }

  /**
   * Closure to allow checking the attrName for the inline event pattern of starting with 'on'
   * @param attrName
   * @returns {*|boolean}
   * @private
   */
  var _isInlineEventAttribute = function (attrName) {
    return attrName && attrName.toString().length > 1 && attrName.toString().toLowerCase().substr(0, 2) === 'on'
  }

  /**
   * Safe console log check function to check if console.log is available before using it
   * @param textToLog
   * @private
   */
  var _logIfAble = function (textToLog) {
    if (typeof console !== 'undefined' && typeof console.error === 'function') {
      console.error(textToLog)
    }
  }
}(window.dbu = window.dbu || {}))
