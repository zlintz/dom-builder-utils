The purpose of this utility is to allow ease of building out 
entire pieces of the the dom without JQuery or a proprietary 
framework using simple node building techniques in vanilla 
javascript with XSS (Cross Site Scripting) in mind.

# Why I built this?
XSS is consistently in the OWASP TOP 10 for security vulnerabilities. For reference, XSS has the following 
rankings in the OSWAP top 10 for the relative years below. 

Reference the OWASP [Comparison of 2003, 2004, 2007, 2010 and 2013 Releases](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&cad=rja&uact=8&ved=0ahUKEwiZnt2FkK3LAhXCmoMKHS5OBfMQFggrMAI&url=https%3A%2F%2Fraw.githubusercontent.com%2Fcmlh%2FOWASP-Top-Ten-2010%2FRelease_Candidate%2FOWASP_Top_Ten_-_Comparison_of_2003%2C_2004%2C_2007%2C_2010_and_2013_Releases-RC1.pdf&usg=AFQjCNE2kG7FF78TB4zGAvseoeMmIsEgbw&sig2=YBlH7sjMMXU9l_x4jsTgfA)

| 2001 | 2004 | 2007 | 2010 | 2013 |
| -------- | -------- | -------- | -------- | -------- |
| 4th | 4th  | 1st | 2nd| 3rd |

This vulnerability cause cause great damage to a company, their website, 
and ultimately anyone using their site. 

How would you like your 
website to be the source of a keylogger getting placed onto someone's machine
in turn exposing their personal usernames and passwords to say facebook, emails, or 
even their bank account? Or how would you like it if someone injected a little javascript into 
your website that effectively redirected or linked to a pornographic site? 
Or even just defaced your website?

Some of the reasons this is such a problem is...
1. A lack of security training concerning web security
2. Not understanding the negative impact XSS can have on a website and its users
3. Poor coding practices that allow the vulnerability to manifest
4. Last but not least, LAZY developers doing what most tutorials teach in regards to using javascript for DOM manipulation

# Why I built it the way I did?
While I may not be able to address someones lack of security training and the understanding of XSS's negitive impact,
I believe I can help promote good coding practices and provide a utility/tool, if used, that can 
ease building out pieces of the DOM dynamically while minimizing the risk of XSS.

For example, that is why there is a check when an anchor tag is created to check wheather it starts with 'javascript' because
if a link has that as the starting text in the href attribute, javascript can be executed.
If someone, other than the owner of the website, can get their javascript to execute your at risk.



